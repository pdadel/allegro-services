package module;

import AllegroWebApi.ServicePort;
import com.google.common.base.Throwables;
import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import service.AllegroServiceProvider;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Przemyslaw Dadel
 */
public class AllegroModule extends AbstractModule {
    @Override
    protected void configure() {
        Names.bindProperties(binder(), properties());
        bind(ServicePort.class).toProvider(AllegroServiceProvider.class).in(Singleton.class);
    }

    private Properties properties() {
        final StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(System.getProperty("org.module.http.encoding"));
        final EncryptableProperties properties = new EncryptableProperties(encryptor);
        try {
            properties.load(ClassLoader.getSystemResource("allegro.properties").openStream());
            return properties;
        } catch (IOException e) {
            throw Throwables.propagate(e);
        }

    }
}
