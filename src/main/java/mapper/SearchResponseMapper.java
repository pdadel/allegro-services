package mapper;

import AllegroWebApi.ItemsListType;
import service.SearchQuery;

import java.util.Optional;

/**
 * @author Przemyslaw Dadel
 */
public interface SearchResponseMapper<T> {

    Optional<T> map(ItemsListType response, SearchQuery query);

}
