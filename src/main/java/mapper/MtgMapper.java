package mapper;

import AllegroWebApi.ItemsListType;
import AllegroWebApi.PriceInfoType;
import match.SearchResult;
import service.SearchQuery;

import java.util.Optional;

/**
 * @author Przemyslaw Dadel
 */
public class MtgMapper implements SearchResponseMapper<SearchResult> {

    @Override
    public Optional<SearchResult> map(ItemsListType response, SearchQuery query) {
        if (isBuyNowOffer(response)) {
            return Optional.of(new SearchResult(response.getItemTitle(),
                    buyNowPrice(response), response.getSellerInfo().getUserLogin(), query));
        }

        return Optional.empty();
    }

    private float buyNowPrice(ItemsListType response) {
        final PriceInfoType[] priceInfo = response.getPriceInfo();
        for (PriceInfoType priceInfoType : priceInfo) {
            final String priceType = priceInfoType.getPriceType();
            if (priceType.equals("buyNow")) {
                return priceInfoType.getPriceValue();
            }
        }
        return 0;

    }

    private boolean isBuyNowOffer(ItemsListType response) {
        return true;
    }

}
