package runner;

import com.google.inject.Guice;
import com.google.inject.Injector;
import mapper.MtgMapper;
import match.SearchResult;
import match.SearchSummary;
import match.SellerSummary;
import module.AllegroModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.SearchQuery;
import service.SearchService;
import service.SessionId;
import service.SessionProvider;

import javax.xml.rpc.ServiceException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Przemyslaw Dadel
 */
public class Go {

    private static final Logger LOGGER = LoggerFactory.getLogger(Go.class);

    public static void main(String[] args) throws ServiceException, IOException {
        final Injector injector = Guice.createInjector(new AllegroModule());

        final SessionProvider sessionProvider = injector.getInstance(SessionProvider.class);
        final SearchService searchService = injector.getInstance(SearchService.class);
        final SessionId session = sessionProvider.login();

        final List<String> items = Files
                .readAllLines(Paths.get(args[0]))
                .stream()
                .map(String::trim)
                .filter(n -> !(n.isEmpty() || n.startsWith("#")))
                .sorted()
                .distinct()
                .collect(Collectors.<String>toList());

        final int categoryId = 6066;

        final List<SearchResult> results = new ArrayList<>();
        final MtgMapper mapper = new MtgMapper();
        for (String item : items) {
            if (item.startsWith("#")) continue;
            LOGGER.info("Looking for:  " + item);
            final SearchQuery query = new SearchQuery(Integer.toString(categoryId), item);
            results.addAll(searchService.search(session, query, mapper));
        }

        final List<SellerSummary> sellerSummaries = createSellerSummary(results);
        for (SellerSummary sellerSummary : sellerSummaries) {
            LOGGER.info(sellerSummary.seller());
            for (SearchResult result : sellerSummary.items()) {
                LOGGER.info("   " + result);
            }
        }

        final List<SearchSummary> searchSummaries = createSearchSummary(results);
        for (SearchSummary searchSummary : searchSummaries) {
            LOGGER.info(searchSummary.query().searchName());
            for (SearchResult result : searchSummary.items()) {
                LOGGER.info("   " + result);
            }
        }

    }

    private static List<SellerSummary> createSellerSummary(List<SearchResult> results) {
        final Map<String, List<SearchResult>> collect = results.stream().collect(Collectors.groupingBy(r -> r.getSellerName()));
        final Stream<SellerSummary> summaries = collect.entrySet().stream().map(entry -> new SellerSummary(entry.getKey(), entry.getValue()));
        final Stream<SellerSummary> sorted = summaries.sorted(Comparator.comparingInt(summary -> summary.items().size()));
        return sorted.collect(Collectors.toList());
    }

    private static List<SearchSummary> createSearchSummary(List<SearchResult> results) {
        final Map<SearchQuery, List<SearchResult>> byName = results.stream().collect(Collectors.groupingBy(r -> r.getQuery()));
        return byName.entrySet()
                .stream()
                .map(entry -> new SearchSummary(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

}
