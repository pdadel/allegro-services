package match;

import service.SearchQuery;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Przemyslaw Dadel
 */
public class SearchSummary {

    private final SearchQuery query;
    private final List<SearchResult> searchResults;

    public SearchSummary(SearchQuery query, List<SearchResult> searchResults) {
        this.query = query;
        this.searchResults = searchResults
                .stream()
                .sorted((o1, o2) -> Float.compare(o1.getBuyNowPrice(), o2.getBuyNowPrice()))
                .collect(Collectors.toList());
    }

    public SearchQuery query() {
        return query;
    }

    public List<SearchResult> items() {
        return searchResults;
    }
}
