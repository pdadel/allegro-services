package match;

import java.util.List;

/**
 * @author Przemyslaw Dadel
 */
public class SellerSummary {

    private final String seller;
    private final List<SearchResult> searchResults;

    public SellerSummary(String seller, List<SearchResult> searchResults) {
        this.seller = seller;
        this.searchResults = searchResults;
    }

    public String seller() {
        return seller;
    }

    public List<SearchResult> items() {
        return searchResults;
    }
}
