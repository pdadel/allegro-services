package match;

import service.SearchQuery;

/**
 * @author Przemyslaw Dadel
 */
public class SearchResult {

    private final String name;
    private final float buyNowPrice;
    private final String sellerName;
    private final SearchQuery query;

    public SearchResult(String name, float buyNowPrice, String sellerName, SearchQuery query) {
        this.name = name;
        this.buyNowPrice = buyNowPrice;
        this.sellerName = sellerName;
        this.query = query;
    }

    public float getBuyNowPrice() {
        return buyNowPrice;
    }

    public String getSellerName() {
        return sellerName;
    }


    public SearchQuery getQuery() {
        return query;
    }

    @Override
    public String toString() {
        return String.format("%-50s - %5.2f", name, buyNowPrice);
    }
}
