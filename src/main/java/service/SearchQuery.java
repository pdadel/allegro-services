package service;

import com.google.common.base.Objects;

/**
 * @author Przemyslaw Dadel
 */
public class SearchQuery {

    private final String categoryId;
    private final String searchName;

    public SearchQuery(String categoryId, String searchName) {
        this.categoryId = categoryId;
        this.searchName = searchName;
    }

    public String categoryId() {
        return categoryId;
    }

    public String searchName() {
        return searchName;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("categoryId", categoryId)
                .add("searchName", searchName)
                .toString();
    }
}
