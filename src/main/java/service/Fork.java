package service;

import fork.CountingTask;

import java.nio.file.Paths;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

/**
 * @author Przemyslaw Dadel
 */
public class Fork {

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        final ForkJoinPool pool = new ForkJoinPool();

        final ForkJoinTask<Integer> countTaskFuture = pool.submit(new CountingTask(Paths.get(".")));


        System.out.println(countTaskFuture.get());

    }
}
