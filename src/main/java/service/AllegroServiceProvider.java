package service;

import AllegroWebApi.ServicePort;
import AllegroWebApi.ServiceServiceLocator;
import com.google.common.base.Throwables;
import com.google.inject.Provider;

import javax.inject.Singleton;
import javax.xml.rpc.ServiceException;

/**
 * @author Przemyslaw Dadel
 */
@Singleton
public class AllegroServiceProvider implements Provider<ServicePort> {

    @Override
    public ServicePort get() {
        final ServiceServiceLocator locator = new ServiceServiceLocator();
        try {
            return locator.getservicePort();
        } catch (ServiceException e) {
            throw Throwables.propagate(e);
        }
    }

}
