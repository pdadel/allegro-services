package service;

/**
 * @author Przemyslaw Dadel
 */
public class SessionId {

    private final String key;
    private final String webKey;
    private final int countryCode;

    public SessionId(String key, String webkey, int countryCode) {
        this.key = key;
        this.webKey = webkey;
        this.countryCode = countryCode;
    }

    public String key() {
        return key;
    }

    public String webKey() {
        return webKey;
    }

    public int countryCode() {
        return countryCode;
    }
}
