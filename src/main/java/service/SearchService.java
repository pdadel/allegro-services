package service;

import AllegroWebApi.DoGetItemsListRequest;
import AllegroWebApi.DoGetItemsListResponse;
import AllegroWebApi.FilterOptionsType;
import AllegroWebApi.ItemsListType;
import AllegroWebApi.ServicePort;
import com.google.common.base.Stopwatch;
import com.google.common.collect.ImmutableList;
import mapper.SearchResponseMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author Przemyslaw Dadel
 */
@Singleton
public class SearchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchService.class);

    private final ServicePort allegroService;

    @Inject
    public SearchService(ServicePort allegroService) {
        this.allegroService = allegroService;
    }

    public <T> Collection<T> search(SessionId session, SearchQuery query, SearchResponseMapper<T> mapper) throws RemoteException {
        LOGGER.debug("Executing query: {}", query);
        final Stopwatch queryDuration = Stopwatch.createStarted();

        final DoGetItemsListRequest parameters = new DoGetItemsListRequest();
        parameters.setWebapiKey(session.webKey());
        parameters.setCountryId(session.countryCode());
        parameters.setFilterOptions(filterOptions(query));
        final DoGetItemsListResponse results = allegroService.doGetItemsList(parameters);
        LOGGER.debug("Query {} returned {} items in {} ms", query, results.getItemsCount(), queryDuration.elapsed(TimeUnit.MILLISECONDS));

        final Collection<T> result = new ArrayList<>(results.getItemsCount());
        if (results.getItemsCount() > 0) {
            for (ItemsListType response : results.getItemsList()) {
                final Optional<T> map = mapper.map(response, query);
                map.ifPresent(mapped -> result.add(mapped));
            }
        }
        LOGGER.debug("Query result mapped to {} items", result.size());
        return result;
    }

    private FilterOptionsType[] filterOptions(SearchQuery query) {

        final FilterOptionsType category = categoryFilter(query);
        final FilterOptionsType search = searchFilter(query);
        final ImmutableList<FilterOptionsType> filters =
                new ImmutableList.Builder<FilterOptionsType>()
                        .add(category)
                        .add(search)
                        .build();


        return filters.toArray(new FilterOptionsType[filters.size()]);
    }

    private FilterOptionsType searchFilter(SearchQuery query) {
        final FilterOptionsType type1 = new FilterOptionsType();
        type1.setFilterId("search");
        type1.setFilterValueId(new String[]{query.searchName()});
        return type1;
    }

    private FilterOptionsType categoryFilter(SearchQuery query) {
        final FilterOptionsType type = new FilterOptionsType();
        type.setFilterId("category");
        type.setFilterValueId(new String[]{query.categoryId()});
        return type;
    }


}
