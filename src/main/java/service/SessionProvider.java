package service;

import AllegroWebApi.DoLoginRequest;
import AllegroWebApi.DoLoginResponse;
import AllegroWebApi.DoQuerySysStatusRequest;
import AllegroWebApi.DoQuerySysStatusResponse;
import AllegroWebApi.ServicePort;
import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.rmi.RemoteException;
import java.util.concurrent.TimeUnit;

/**
 * @author Przemyslaw Dadel
 */
@Singleton
public class SessionProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(SessionProvider.class);

    private final ServicePort allegroService;
    private final String user;
    private final String password;
    private final String webkey;
    private final int countryCode;

    @Inject
    public SessionProvider(ServicePort allegroService,
                           @Named("allegro.user") String user,
                           @Named("allegro.password") String password,
                           @Named("allegro.webkey") String webkey,
                           @Named("allegro.country") int countryCode) {
        this.allegroService = allegroService;
        this.user = user;
        this.password = password;
        this.webkey = webkey;
        this.countryCode = countryCode;
    }

    public SessionId login() throws RemoteException {
        LOGGER.info("Login with user {}", user);
        LOGGER.debug("Updating component version");

        final Stopwatch login = Stopwatch.createStarted();
        final long apiVersion = retrieveApiVersion();
        LOGGER.debug("Component version updated");

        final DoLoginRequest loginRequest = new DoLoginRequest();
        loginRequest.setUserLogin(user);
        loginRequest.setUserPassword(password);
        loginRequest.setCountryCode(countryCode);
        loginRequest.setWebapiKey(webkey);
        loginRequest.setLocalVersion(apiVersion);

        final DoLoginResponse loginResponse = allegroService.doLogin(loginRequest);
        final String sessionId = loginResponse.getSessionHandlePart();
        LOGGER.info("Login completed in: {} ms", login.elapsed(TimeUnit.MILLISECONDS));
        return new SessionId(sessionId, webkey, countryCode);
    }

    private long retrieveApiVersion() throws RemoteException {
        final DoQuerySysStatusRequest statusRequest = new DoQuerySysStatusRequest();
        statusRequest.setCountryId(countryCode);
        statusRequest.setWebapiKey(webkey);
        statusRequest.setSysvar(4);
        final DoQuerySysStatusResponse statusResponse = allegroService.doQuerySysStatus(statusRequest);
        return statusResponse.getVerKey();
    }

}
